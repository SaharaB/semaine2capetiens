// P  p

let express = require("express");
let app = express();// fontion pour cree le serveur
var fs = require('fs');
var bodyParser = require("body-parser"); 
var urlencodedParser = bodyParser.urlencoded({ extended : false});
app.listen(8080);

app.set('view engine', 'ejs');

app.get("/", function (request, response) {
  response.render("sem2capetiens", { page: "serveur.js" });
});

app.get("/hugues", function (request, response) {
  response.render("hugues");
});

app.get("/louis", (request, response) => {
  response.render("louis");
});

app.get("/auguste", (request, response) => {
  response.render("auguste");
});

app.get("/lebel", function (request, response)  {
  response.render("lebel");
});

app.get("/formulaire", (request, response) => {
  response.render("formulaire");
});

// app.use(express.static(path.join(__dirname, 'formulaire')))
// app.use('/static', express.static(__dirname + '/formulaire'));


app.use(express.static("public"));


app.get("*", function (request, response) {
  response.status(404).send("Not found");// *(404) doit etre a la fin de toutes les routes
});

app.post("/formulaire", urlencodedParser, function (request, response) {
form = request.body;
var data = JSON.stringify(form, null, 2);
fs.appendFile('sem2c.json', data, (err) => {
  if (err) throw err;
});
});





